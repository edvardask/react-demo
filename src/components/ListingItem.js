import React, {Component} from 'react';

class ListingItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inCart: false
        };

        this.addToCart = this.addToCart.bind(this);
    }

    addToCart() {
        this.setState((prevState) => {
            return {
                inCart: !prevState.inCart
            }
        });
    }

    render() {
        return (
            <li>{this.props.title}
                <button
                    onClick={this.addToCart}>
                    {this.state.inCart ? 'Already in cart' : 'Add to cart'}
                </button>
            </li>
        );
    }
}

export default ListingItem;