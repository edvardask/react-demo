import React, {Component} from 'react';
import {Store} from "../store/index";
import ListingItem from "./ListingItem";

class Listing extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            items: []
        };

        this.addRandomItem = this.addRandomItem.bind(this);
        this.init = this.init.bind(this);

    }

    componentDidMount() {
        this.init();

        let $c = this;

        Store.subscribe((items) => {
            $c.setState({
                items: items,
                loading: false
            });
        })
    }

    init() {
        let _this = this;

        this.setState(() => {
            return {
                loading: true
            }
        });

        Store.getItems().then((items) => {
            _this.setState(() => {
                return {
                    items: items,
                    loading: false
                }
            })
        });
    }

    addRandomItem() {
        let randomId = Math.round(Math.random() * 100);

        this.setState({
            loading: true
        });

        Store.addItem('Random item ' + randomId);
    }

    className() {
        let classes = ['listing'];

        if (this.state.loading) {
            classes.push('loading');
        }

        return classes.join(' ');
    }

    render() {
        return (
            <div className={this.className()}>
                <h1>Shop demo</h1>
                <ul>
                    {this.state.items.map((item) => {
                        return <ListingItem title={item.title} key={item.id}/>
                    })}
                </ul>
                {!this.state.loading ?
                    <button onClick={this.addRandomItem}>
                        Add Random Item
                    </button>
                    : <h2>Loading, please wait</h2>
                }
            </div>
        );
    }
}

export default Listing;