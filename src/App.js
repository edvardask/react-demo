import React, {Component} from 'react';
import './App.css';
import Listing from './components/Listing';
import {Store} from './store/index';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0,
            updates: 0
        }
    }

    componentDidMount() {
        let $c = this;

        Store.subscribe((items) => {
            $c.setState((prevState) => {
                return {
                    count: items.length,
                    updates: prevState.updates + 1
                }
            })
        });
    }

    render() {
        return (
            <div className="App">
                <Listing/>
                <p>Item count: {this.state.count}</p>
                <p>Updates: {this.state.updates}</p>
            </div>
        );
    }
}

export default App;