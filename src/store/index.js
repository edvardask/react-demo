import * as actions from './actions';

let items = [
    {
        id: 1,
        title: 'Bike'
    },
    {
        id: 2,
        title: 'Car'
    },
    {
        id: 3,
        title: 'Plane'
    }
];

let subscribers = [];

export const Store = {
    methods: {
        [actions.ITEMS]() {
            return new Promise((resolve) => {
                setTimeout(() => {
                    //Notify subscribers
                    for (let i = 0, ls = subscribers.length; i < ls; i++) {
                        subscribers[i](items);
                    }

                    resolve(items);
                }, 1000);
            });
        }
    },
    addItem(title) {
        items = [...items, {
            id: items.length + 1,
            title: title
        }];

        //Refresh items after one was added
        this.methods[actions.ITEMS]();
    },
    getItems() {
        return this.methods[actions.ITEMS]();
    },
    subscribe(callback) {
        subscribers.push(callback)
    }
};