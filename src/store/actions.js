export const ADD_TO_CART = 'add_to_cart';
export const ITEMS = 'items';
export const REMOVE_FROM_CART = 'remove_from_cart';